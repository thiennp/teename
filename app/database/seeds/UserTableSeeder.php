<?php

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();

		User::create(array(
			'role'     => 0,
			'uid'     => "",
			'email'    => 'nguyenphongthien@gmail.com',
			'password' => Hash::make('thienkimtuoc'),
			'first_name' => 'Thien',
			'last_name' => 'Nguyen',
			'name'     => 'Thien Nguyen',
			'gender' => 'male'
		));

		User::create(array(
			'role'     => 0,
			'uid'     => "",
			'email'    => 'kuong.bien@gmail.com',
			'password' => Hash::make('admin123456'),
			'first_name' => 'Cuong',
			'last_name' => 'Pham',
			'name'     => 'Cuong Pham',
			'gender' => 'male'
		));
	}

}