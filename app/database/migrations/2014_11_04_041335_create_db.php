<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDb extends Migration {

	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->tinyInteger('role');
			$table->string('uid');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('name');
			$table->string('gender',10);
			$table->string('locale');
			$table->string('link');
			$table->integer('timezone');
			$table->boolean('actived');
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});

		Schema::create('campaign', function($table)
		{
			$table->increments('id');
			$table->integer('style');
			$table->string('copy');
			$table->string('url');
			$table->boolean('actived');
			$table->timestamps();
		});

		Schema::create('campaign_users', function($table)
		{
			$table->increments('id');
			$table->integer('user_id');
			$table->string('color', 6);
			$table->foreign('user_id')
				->references('id')->on('users')
				->onDelete('cascade');
			$table->integer('campaign_id');
			$table->foreign('campaign_id')
				->references('id')->on('campaign')
				->onDelete('cascade');
			$table->integer('type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('users');
		Schema::dropIfExists('campaign');
		Schema::dropIfExists('campaign_users');
	}
}