<?php

class HomeController extends BaseController {
	public function config() {
		include(app_path().'/../config.php');
		$string = file_get_contents(app_path().'/../config.json');
		$json_config = json_decode($string);

		$google_font_list = [];
		foreach ($json_config->copies as $key => $copy) {
			$fonts = explode(',', $copy->custom->font);
			foreach ($fonts as $key1 => $font) {
				$font = trim($font);
				$font = trim($font,"'");
				$font = str_replace(' ', '+', $font);
				$check = true;
				foreach ($google_font_list as $key2 => $google_font) {
					if ($google_font == $font) {
						$check = false;
					}
				}
				if ($check) {
					array_push($google_font_list, $font);
				}
			}
		};

		return array
		(
			'googlefonts' => $google_font_list,
			'base' => $base,
			'appid' => $appid,
			'copies' => $json_config->copies,
			'types' => $json_config->types,
			'gacode' => $json_config->gacode
		);
	}

	public function campaignImage($campaigns) {
		$images = array();
		foreach ($campaigns as $key => $value) {
			$copy = $this->letterCalculate($value['copy']);
			if (!isset($images[$copy])) {
				$images[$copy] = array();
				$images[$copy][$value['style']] = array();
			}
			if (isset($value['links']->link_1))	$images[$copy][$value['style']][0] = $value['links']->link_1;
			if (isset($value['links']->link_2))	$images[$copy][$value['style']][1] = $value['links']->link_2;
			if (isset($value['links']->link_3))	$images[$copy][$value['style']][2] = $value['links']->link_3;
		}
		return $images;
	}

	public function campaignUrl($campaigns) {
		$urls = array();
		foreach ($campaigns as $key => $value) {
			if ($value['url'] != '') {
				$copy = $this->letterCalculate($value['copy']);
				if (!isset($urls[$copy])) $urls[$copy] = array();
				$urls[$copy][$value['style']] = $value['url'];
			}
		}
		return $urls;
	}

	public function doLogin() {
		if (Auth::check())
		{
			return Redirect::to('ktadmin');
		}
		else
		{
			include(app_path().'/../config.php');
			$username = Input::get('username');
			$password = Input::get('password');
			if ($username == $admin_user && $password == $admin_pass) {
				Auth::loginUsingId($admin_id);
				return Redirect::to('ktadmin');
			} else {
				$config_data = $this->config();
				return View::make('login', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'error' => TRUE));
			}
		}
	}

	public function login() {
		if (Auth::check())
		{
			return Redirect::to('ktadmin');
		}
		else
		{
			$config_data = $this->config();
			return View::make('login', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'error' => FALSE));
		}
	}

	public function logout() {
		if (Auth::check())
		{
			Auth::logout();
		}
		return Redirect::to('login');
	}

	public function admin() {
		if (Auth::check())
		{
			$config_data = $this->config();
			$campaigns = Campaign::all();
			foreach ($campaigns as $key => $value) {
				if ($value->actived == 0) $value->actived = NULL;
				else $value->actived = 0;
				if ($value->description) {
					$campaigns[$key]->links = json_decode($value->description);
				} else {
					$campaigns[$key]->links = (object)NULL;
					$campaigns[$key]->links->link_1 = '';
					$campaigns[$key]->links->link_2 = '';
					$campaigns[$key]->links->link_3 = '';
				}
				$users = DB::table('campaign_users')->where('campaign_id', '=', $value->id)->count(DB::raw('DISTINCT user_id'));
				$campaigns[$key]->count = $users;
			}
			$campaigns = $campaigns->sortByDesc('count')->sortBy('url');
			return View::make('admin', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'campaigns' => $campaigns, 'copies' => $config_data['copies'], 'types' => $config_data['types']));
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function manual($route) {
		$config_data = $this->config();
		echo '<script>top.window.location.href="http://goo.gl/'.$route.'";</script>';
	}

	public function adminCampaign($id) {
		if (Auth::check())
		{
			$config_data = $this->config();
			$campaign_users = DB::table('campaign_users')->where('campaign_id', $id)->get();
			$users = array();
			foreach ($campaign_users as $key => $value) {
				$user = DB::table('users')->where('id', $value->user_id)->first();
				if ($user) {
					$duplicated = FALSE;
					foreach ($users as $user_key => $user_value) {
						if ($user_value->id == $user->id) {
							$duplicated = TRUE;
							break;
						}
					}
					if (!$duplicated) array_push($users, $user);
				}
			}
			return View::make('admin_campaign', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'campaign_users' => $users));
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function adminConfig() {
		if (Auth::check())
		{
			$config_data = $this->config();
			$string = file_get_contents(app_path().'/../config.json');
			$json_config = json_decode($string);
			foreach ($json_config->copies as $key => $value) {
				if (!isset($value->delete)) {
					$json_config->copies[$key]->delete = false;
				}
			}
			return View::make('admin_config', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'json_config' => $json_config));
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function adminCopy() {
		if (Auth::check())
		{
			$config_data = $this->config();
			$copies = DB::table('copy_addition')->get();
			return View::make('admin_copy', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'copies' => $copies));
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function adminUser() {
		if (Auth::check())
		{
			$config_data = $this->config();
			$users = DB::table('users')->get();
			return View::make('admin_user', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'users' => $users));
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function updateConfig() {
		if (Auth::check())
		{
			$json = (object)NULL;
			$json->copies = array();
			$key = 0;
			$json->gacode = Input::get('gacode');
			while (Input::has('width_'.$key)) {
				$json->copies[$key] = (object)NULL;
				$json->copies[$key]->width = Input::get('width_'.$key);
				$json->copies[$key]->left = Input::get('left_'.$key);
				$json->copies[$key]->top = Input::get('top_'.$key);
				if(Input::get('delete_custom_'.$key)) {
					$json->copies[$key]->delete = true;
				} else {
					$json->copies[$key]->delete = false;
				}
				$json->copies[$key]->custom = (object)NULL;
				$json->copies[$key]->custom->font = Input::get('font_custom_'.$key);
				$json->copies[$key]->custom->size = Input::get('size_custom_'.$key);
				$json->copies[$key]->custom->width = Input::get('width_custom_'.$key);
				$json->copies[$key]->custom->rotate = Input::get('rotate_custom_'.$key);
				$json->copies[$key]->custom->left = Input::get('left_custom_'.$key);
				$json->copies[$key]->custom->top = Input::get('top_custom_'.$key);
				if (Input::file('img_copy_'.$key)) {
					$file = Input::file('img_copy_'.$key)->move(public_path(), 'copy-'.($key+1).'.png');
				}
				$key++;
			}
			if (Input::file('img_copy_add')) {
				$json->copies[$key] = (object)NULL;
				$json->copies[$key]->width = 70;
				$json->copies[$key]->left = 62;
				$json->copies[$key]->top = 35;
				$json->copies[$key]->delete = false;
				$json->copies[$key]->custom = (object)NULL;
				$json->copies[$key]->custom->font = "'Lobster', cursive";
				$json->copies[$key]->custom->size = NULL;
				$json->copies[$key]->custom->width = 150;
				$json->copies[$key]->custom->rotate = -8;
				$json->copies[$key]->custom->left = 12;
				$json->copies[$key]->custom->top = 66;
				$file = Input::file('img_copy_add')->move(public_path(), 'copy-'.($key+1).'.png');
			}
			$key = 0;
			while (Input::has('price_'.$key)) {
				$json->types[$key] = (object)NULL;
				$json->types[$key]->top = Input::get('top_type_'.$key);
				$json->types[$key]->price = Input::get('price_'.$key);
				$json->types[$key]->description = Input::get('description_'.$key);
				$json->types[$key]->colors = array();
				$color_key = 0;
				while (Input::has('color_'.$key.'_'.$color_key)) {
					$json->types[$key]->colors[$color_key] = Input::get('color_'.$key.'_'.$color_key);
					$color_key++;
				}
				if (Input::file('img_copy_add')) {
					$json->types[$key]->colors[$color_key] = Input::get('color_'.$key.'_0');
				}
				$key++;
			}
			$string = json_encode($json);
			file_put_contents(app_path().'/../config.json', $string);
			return Redirect::to('ktadmin/config');
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function showIndex() {
		$config_data = $this->config();
		$campaigns = Campaign::all();
		foreach ($campaigns as $key => $value) {
			if ($value->description) {
				$campaigns[$key]->links = json_decode($value->description);
			} else {
				$campaigns[$key]->links = (object)NULL;
				$campaigns[$key]->links->link_1 = '';
				$campaigns[$key]->links->link_2 = '';
				$campaigns[$key]->links->link_3 = '';
			}
		}
		$urls = $this->campaignUrl($campaigns);
		$images = $this->campaignImage($campaigns);
		return View::make('index', array('added_copies' => array(), 'current_campaign_name' => NULL, 'current_name' => NULL, 'changed_name' => NULL, 'first_name' => NULL, 'last_name' => NULL, 'last_campaign_name' => NULL, 'googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'appid' => $config_data['appid'], 'copies' => $config_data['copies'], 'types' => $config_data['types'], 'campaigns' => $campaigns, 'urls' => $urls, 'images' => $images));
	}

	public function showCopy($changed_name, $first_name, $last_name) {
		$changed_name = ucfirst($changed_name);
		$config_data = $this->config();
		$campaigns = Campaign::all();
		$added_copies = DB::table('copy_addition')
			->where('copy', $this->letterCalculate($changed_name))
			->get();
		foreach ($campaigns as $key => $value) {
			$value->copy = $this->letterCalculate($value->copy);
			if ($value->description) {
				$campaigns[$key]->links = json_decode($value->description);
			} else {
				$campaigns[$key]->links = (object)NULL;
				$campaigns[$key]->links->link_1 = '';
				$campaigns[$key]->links->link_2 = '';
				$campaigns[$key]->links->link_3 = '';
			}
		}
		$urls = $this->campaignUrl($campaigns);
		$images = $this->campaignImage($campaigns);
		return View::make('index', array('added_copies' => $added_copies, 'current_campaign_name' => $this->letterCalculate($changed_name), 'current_name' => $changed_name, 'changed_name' => $changed_name, 'first_name' => $first_name, 'last_campaign_name' => $this->letterCalculate($last_name), 'last_name' => $last_name, 'googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'appid' => $config_data['appid'], 'copies' => $config_data['copies'], 'types' => $config_data['types'], 'campaigns' => $campaigns, 'urls' => $urls, 'images' => $images));
	}

	public function showCopyFromShare() {
		$copy = $_REQUEST['copy'];
		return Redirect::to('copy/'.$copy.'/firstname/lastname');
	}

	public function showName($first_name, $last_name) {
		$first_name = ucfirst($this->letterCalculate($first_name));
		$last_name = ucfirst($this->letterCalculate($last_name));
		$last_name_fix = str_replace('-', ' ', $last_name);
		$last_name_array = explode(' ', $last_name_fix);
		$added_copies = DB::table('copy_addition')
			->where('copy', $this->letterCalculate($first_name))
			->get();
		foreach ($last_name_array as $key => $value) {
			$last_name_array[$key] = array();
			$last_name_array[$key]['name'] = ucfirst($value);
			$last_name_array[$key]['campaign_name'] = $this->letterCalculate($value);
			$added_copies = array_merge($added_copies, DB::table('copy_addition')
				->where('copy', $this->letterCalculate($value))
				->get()
			);
		}
		$config_data = $this->config();
		$campaigns = Campaign::all();
		foreach ($campaigns as $key => $value) {
			if ($value->description) {
				$campaigns[$key]->links = json_decode($value->description);
			} else {
				$campaigns[$key]->links = (object)NULL;
				$campaigns[$key]->links->link_1 = '';
				$campaigns[$key]->links->link_2 = '';
				$campaigns[$key]->links->link_3 = '';
			}
		}
		$urls = $this->campaignUrl($campaigns);
		$images = $this->campaignImage($campaigns);
		return View::make('index', array('added_copies' => $added_copies, 'last_name_array' => $last_name_array, 'current_campaign_name' => $this->letterCalculate($first_name), 'current_name' => $first_name, 'changed_name' => NULL, 'first_name' => $first_name, 'last_campaign_name' => $this->letterCalculate($last_name), 'last_name' => $last_name, 'googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode'], 'appid' => $config_data['appid'], 'copies' => $config_data['copies'], 'types' => $config_data['types'], 'campaigns' => $campaigns, 'urls' => $urls, 'images' => $images));
	}

	public function createOrder() {
		$config_data = $this->config();
		$copy = ucfirst($this->letterCalculate(Input::get('copy')));
		$first_name = Input::get('first_name');
		$last_name = Input::get('last_name');
		$name = Input::get('name');
		$email = Input::get('email');
		$gender = Input::get('gender');
		$locale = Input::get('locale');
		$link = Input::get('link');
		$timezone = Input::get('timezone');
		$uid = Input::get('uid');
		$style = Input::get('style');
		$color = Input::get('color');

		$campaign = Campaign::whereRaw('`copy` = ? and `style` = ? and `actived` = ?', array($copy, $style, 0))->take(1)->get();
		$user = User::whereRaw('`uid` = ?', array($uid))->get();

		if (count($user) > 0) {
			$user_id = $user[0]['id'];
		} else {
			$user = new User;
			$user->first_name = $first_name;
			$user->last_name = $last_name;
			$user->name = $name;
			$user->email = $email;
			$user->gender = $gender;
			$user->locale = $locale;
			$user->link = $link;
			$user->timezone = $timezone;
			$user->uid = $uid;
			$user->save();
			$user_id = $user->id;
		}
		if (count($campaign) > 0) {
			$campaign_id = $campaign[0]['id'];
			$campaign_users = CampaignUsers::whereRaw('`user_id` = ? and `color` = ? and `campaign_id` = ?', array($user_id, $color, $campaign_id))->take(1)->get();
			if (count($campaign_users) == 0) {
				$campaign_users = new CampaignUsers;
				$campaign_users->campaign_id = $campaign[0]['id'];
				$campaign_users->user_id = $user_id;
				$campaign_users->save();
			}
			if ($campaign[0]['url'] != '') {
				return Redirect::to('manual/'.$campaign[0]['url']);
			}
		} else {
			$campaign = new Campaign;
			$campaign->copy = $copy;
			$campaign->style = $style;
			$campaign->save();

			$campaign_users = new CampaignUsers;
			$campaign_users->campaign_id = $campaign->id;
			$campaign_users->user_id = $user_id;
			$campaign_users->save();
		}
		return View::make('success', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode']));
	}

	public function success() {
		$config_data = $this->config();
		return View::make('success', array('googlefonts'=> $config_data['googlefonts'], 'base' => $config_data['base'], 'gacode' => $config_data['gacode']));
	}

	public function updateCampaign($id) {
		if (Auth::check())
		{
			$url = Input::get('url');
			$actived = Input::get('actived');
			$description = '{';
			$tmp = (object)NULL;
			for ($key=1; $key<=3; $key++) {
				$tmp_attr = 'link_'.$key;
				$tmp->$tmp_attr = Input::get('img_custom_'.$key);
			}
			$description = json_encode($tmp);
			if ($actived == NULL) $actived = 0;
			else $actived = 1;
			DB::table('campaign')
				->where('id', $id)
				->update(array('url' => $url, 'actived' => $actived, 'description' => $description));
			return Redirect::to('ktadmin');
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function updateCopy($id) {
		if (Auth::check())
		{
			$copy = $this->letterCalculate(Input::get('copy'));
			$add_image = Input::get('add_image');
			$add_link = Input::get('add_link');
			$price = Input::get('price');
			$order = Input::get('order');
			$description = Input::get('description');
			if ($id == 0) {
				DB::table('copy_addition')
					->insert(array('copy' => $copy, 'add_image' => $add_image, 'add_link' => $add_link, 'price' => $price, 'description' => $description, 'order' => $order));
			} else {
				if(Input::get('delete')) {
					DB::table('copy_addition')
						->where('id', $id)
						->delete();
				} else {
					DB::table('copy_addition')
						->where('id', $id)
						->update(array('add_image' => $add_image, 'add_link' => $add_link, 'price' => $price, 'description' => $description, 'order' => $order));
				}
			}
			return Redirect::to('ktadmin/copy');
		}
		else
		{
			return Redirect::to('login');
		}
	}

	public function letterCalculate($string) {
		$encodeStr = array('áº£', 'Ã£', 'áº¡', 'Äƒ', 'áº±', 'áº¯', 'áº³', 'áºµ', 'áº·', 'Ã¢', 'áº§', 'áº©', 'áº«', 'áº­', 'à', 'á', 'ả', 'ã', 'ạ', 'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ', 'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ', 'Ã€', 'Ã', 'áº¢', 'Ãƒ', 'áº ', 'Ä‚', 'áº°', 'áº®', 'áº²', 'áº´', 'áº¶', 'Ã‚', 'áº¦', 'áº¨', 'áºª', 'áº¬', 'A', 'À', 'Á', 'Ả', 'Ã', 'Ạ', 'Ă', 'Ằ', 'Ắ', 'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Ã¨', 'Ã©', 'áº»', 'áº½', 'áº¹', 'Ãª', 'á»', 'áº¿', 'á»ƒ', 'á»…', 'á»‡', 'Ãˆ', 'Ã‰', 'áºº', 'áº¼', 'áº¸', 'ÃŠ', 'á»€', 'áº¾', 'á»‚', 'á»„', 'á»†', 'è', 'é', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ', 'E', 'È', 'É', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ề', 'Ế', 'Ể', 'Ễ', 'Ệ', 'Ã¬', 'Ã­', 'á»‰', 'Ä©', 'á»‹', 'ÃŒ', 'Ã', 'á»ˆ', 'Ä¨', 'á»Š', 'ì', 'í', 'ỉ', 'ĩ', 'ị', 'I', 'Ì', 'Í', 'Ỉ', 'Ĩ', 'Ị', 'Ã²', 'Ã³', 'á»', 'Ãµ', 'á»', 'Ã´', 'á»“', 'á»‘', 'á»•', 'á»—', 'á»™', 'Æ¡', 'á»', 'á»›', 'á»Ÿ', 'á»¡', 'á»£', 'Ã’', 'Ã“', 'á»Ž', 'Ã•', 'á»Œ', 'Ã”', 'á»’', 'á»', 'á»”', 'á»–', 'á»˜', 'Æ ', 'á»œ', 'á»š', 'á»ž', 'á» ', 'á»¢', 'ò', 'ó', 'ỏ', 'õ', 'ọ', 'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ', 'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ', 'O', 'Ò', 'Ó', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ồ', 'Ố', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ờ', 'Ớ', 'Ở', 'Ỡ', 'Ợ', 'Ã¹', 'Ãº', 'á»§', 'Å©', 'á»¥', 'Æ°', 'á»«', 'á»©', 'á»­', 'á»¯', 'á»±', 'Ã™', 'Ãš', 'á»¦', 'Å¨', 'á»¤', 'Æ¯', 'á»ª', 'á»¨', 'á»¬', 'á»®', 'á»°', 'ù', 'ú', 'ủ', 'ũ', 'ụ', 'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự', 'U', 'Ù', 'Ú', 'Ủ', 'Ũ', 'Ụ', 'Ư', 'Ừ', 'Ứ', 'Ử', 'Ữ', 'Ự', 'á»³', 'Ã½', 'á»·', 'á»¹', 'á»µ', 'á»²', 'Ã', 'á»¶', 'á»¸', 'á»´', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ', 'Y', 'Ỳ', 'Ý', 'Ỷ', 'Ỹ', 'Ỵ');
		$decodeStr = array('a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'u', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y');
		foreach ($encodeStr as $key => $value) {
			$string = str_replace($encodeStr[$key], $decodeStr[$key], $string);
		}
		return strtolower($string);
	}
}
