<?php

Route::any('/', 'HomeController@showIndex');
Route::any('copy/{changed_name}/{first_name}/{last_name}', 'HomeController@showCopy');
Route::any('name/{first_name}/{last_name}', 'HomeController@showName');
Route::any('success', 'HomeController@success');
Route::any('user_existed/{uid}', function($uid) {
	$user = User::whereRaw('`uid` = ?', array($uid))->get();
	if (count($user) > 0) {
		return $user[0]->email;
	} else {
		return NULL;
	}
});

// For facebook canvas share
Route::any('share', 'HomeController@showCopyFromShare');

Route::get('login', 'HomeController@login');
Route::get('logout', 'HomeController@logout');
Route::get('ktadmin', 'HomeController@admin');
Route::get('manual/{route}', 'HomeController@manual');
Route::get('ktadmin/config', 'HomeController@adminConfig');
Route::get('ktadmin/campaign/{campaignid}', 'HomeController@adminCampaign');
Route::get('ktadmin/copy', 'HomeController@adminCopy');
Route::get('ktadmin/user', 'HomeController@adminUser');

Route::post('login', 'HomeController@doLogin');
Route::post('campaign/{id}', 'HomeController@updateCampaign');
Route::post('copy/{id}', 'HomeController@updateCopy');
Route::post('config', 'HomeController@updateConfig');
Route::post('change_user', 'HomeController@changeUser');
Route::post('change_order', 'HomeController@changeOrder');
Route::post('create_order', 'HomeController@createOrder');