		<!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
		<!--[if lt IE 9]>
		{{ HTML::script('global/plugins/respond.min.js') }}
		<![endif]-->
		{{ HTML::script('https://thiepcuoiviet.net/teename/public/global/plugins/jquery.min.js') }}
		{{ HTML::script('https://thiepcuoiviet.net/teename/public/global/plugins/jquery-migrate.min.js') }}
		{{ HTML::script('https://thiepcuoiviet.net/teename/public/global/plugins/bootstrap/js/bootstrap.min.js') }}
		{{ HTML::script('https://thiepcuoiviet.net/teename/public/frontend/layout/scripts/back-to-top.js') }}
		{{ HTML::script('https://thiepcuoiviet.net/teename/public/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
		<!-- END CORE PLUGINS -->
		<script type="text/javascript">
			function validateEmail(email) { 
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(email);
			}
			function showAlert(message) { 
				$("#alert-text").html(message);
				$("#alert").show();
			}
			function hideAlert(message) {
				$("#alert").hide();
			} 
			function orderSubmit() {
				if ($("#email").val() != '') {
					if (validateEmail($("#email").val())) {
						$('#create_order').submit();
					} else {
						showAlert('Email address is invalid');
					}
				} else {
					showAlert('Please enter your email address');
				}
			}
			function handle(e) {
				if(e.keyCode === 13){
					rename();
				}
				return false;
			}
			var campaigns = [];
			@foreach ($campaigns as $key => $value)
				campaigns[{{ $key }}] = {
					'style': "{{ $value->style }}",
					'url': "{{ $value->url }}",
					'copy': "{{ $value->copy }}"
				}
			@endforeach
			jQuery(document).ready(function() {
				window.popupShow = false;
				$(".pi-img-wrapper").on("click", function(){
					window.popupShow = true;
					var scale = 2.4336;
					if ($(this).find(".tee-copy").length > 0) {
						var cp_width = $(this).find(".tee-copy").attr("width")*scale;
						var cp_left = $(this).find(".tee-copy").css("left").split("px")[0]*scale;
						var cp_top = $(this).find(".tee-copy").css("top").split("px")[0]*scale;
						var ct_fontsize = $(this).find(".tee-custom").css("font-size").split("px")[0]*scale;
						var ct_left = $(this).find(".tee-custom").css("left").split("px")[0]*scale;
						var ct_top = $(this).find(".tee-custom").css("top").split("px")[0]*scale;
						var url = null;
						$("#big-image #content").html($(this).html());
						$("#big-image").show();
						$("#big-image #content .tee-shirt").attr("width", 196*scale);
						$("#big-image #content .tee-copy").attr("width", cp_width);
						$("#big-image #content .tee-copy").css({
							'top': cp_top+"px",
							'left': cp_left+"px"
						});
						$("#big-image #content .tee-custom").css({
							'top': ct_top+"px",
							'left': ct_left+"px",
							'font-size': ct_fontsize+"px"
						});
					} else {
						$("#big-image #content").html($(this).html());
						$("#big-image").show();
						$("#big-image .custom-image").css({
							"height": "477px",
							"margin-top": "20px"
						});
					}
				});
				$("#big-image").on("click", function(){
					if (window.popupShow) {
						$("#big-image #content").html("");
						$("#big-image").hide();
					}
				});
			});
			$(window).load(function() {
				firstname();
				@if (!$changed_name)
					lastname();
				@endif;
			})
			var rename = function() {
				$("#copy").val($("#copy").val().trim());
				if ($("#copy").val()) {
					window.location.href = '{{ $base }}copy/'+$("#copy").val()+'/{{ $first_name }}/{{ $last_name }}';
				} else {
					showAlert('Please input a name');
					$("#copy").focus();
				}
			},
			firstname = function() {
				current_name = "{{ $current_name }}";
				var size = [];
				var top = [];
				@foreach ($copies as $key => $value)
					@foreach ($types as $type_key => $type_value)
						size[{{ $key }}] = Math.floor(2*{{ $value->custom->width/4 }}/Math.sqrt(current_name.length));
						top[{{ $key }}] = - size[{{ $key }}] + {{ $value->top+$value->custom->top+$type_value->top }};
						$(".tee-type-{{ $type_key }} .tee-custom-{{ $key }}").css("font-size",size[{{ $key }}]+"px")
						$(".tee-type-{{ $type_key }} .tee-custom-{{ $key }}").css("top",top[{{ $key }}]+"px")
					@endforeach
				@endforeach
				if (["A","E","I","O","U","a","e","i","o","u"].indexOf(current_name[0])>-1) {
					$(".first-name .tee-copy").each(function(){
						$(this).attr("src", $(this).attr("src").split("a.png").join(".png").split(".png").join("a.png"));
					})
				} else {
					$(".first-name .tee-copy").each(function(){
						$(this).attr("src", $(this).attr("src").split("a.png").join(".png"));
					})
				}
			},
			lastname = function() {
				$(".last-name").each(function(){
					var name = '';
					if ($(this).find(".copy-html").html()) {
						name = $(this).find(".copy-html").html().trim();
					}
					$(this).find(".copy-html").html(name);
					$(this).find(".copy-input").val(name);
					var size = [];
					var top = [];
					@foreach ($copies as $key => $value)
						@foreach ($types as $type_key => $type_value)
							size[{{ $key }}] = Math.floor(2*{{ $value->custom->width/4 }}/Math.sqrt(name.length));
							top[{{ $key }}] = - size[{{ $key }}] + {{ $value->top+$value->custom->top+$type_value->top }};
							$(this).find(".tee-type-{{ $type_key }} .tee-custom-{{ $key }}").css("font-size",size[{{ $key }}]+"px")
							$(this).find(".tee-type-{{ $type_key }} .tee-custom-{{ $key }}").css("top",top[{{ $key }}]+"px")
						@endforeach
					@endforeach
					if (["A","E","I","O","U","a","e","i","o","u"].indexOf(name[0])>-1) {
						$(this).find(".tee-copy").each(function(){
							$(this).attr("src", $(this).attr("src").split("a.png").join(".png").split(".png").join("a.png"));
						})
					} else {
						$(this).find(".tee-copy").each(function(){
							$(this).attr("src", $(this).attr("src").split("a.png").join(".png"));
						})
					}
				});
			},
			createorder = function(style, lastname, preventemail){
				$("#style").val(style);
				if (lastname) {
					$("#copy").val(lastname);
				}
				$("#copy").val($("#copy").val().trim());
				$.get("{{ $base }}user_existed/"+$("#uid").val(), function( data ) {
					if(!data) {
						var checked = true;
						for (var i in campaigns) {
							if (campaigns[i].style == $("#style").val() && campaigns[i].copy == $("#copy").val() && campaigns[i].url) {
								checked = false;
								break;
							}
						}
						if (checked && !preventemail) $("#email-prompt").show();
						else $("#create_order").submit();
					} else {
						$("#email").val(data);
						$("#create_order").submit();
					}
				});
			}
		</script>
	</body>
</html>