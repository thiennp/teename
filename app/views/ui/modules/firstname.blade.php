<div class="col-xs-4 col-xs-12">
	<div class="product-item">
		<div class="pi-img-wrapper copy-1 tee-type-{{ $i }}">
			@if (array_key_exists($current_campaign_name,$images))
				@if (array_key_exists($key,$images[$current_campaign_name]))
					@if ($images[$current_campaign_name][$key])
						@if ($images[$current_campaign_name][$key][$i])
							<img src="{{ $images[$current_campaign_name][$key][$i] }}" class="custom-image" style="height: 196px; text-algin: center" >
						@else
							@include('modules/firstname_image')
						@endif
					@else
						@include('modules/firstname_image')
					@endif
				@else
					@include('modules/firstname_image')
				@endif
			@else
				@include('modules/firstname_image')
			@endif
			<!-- Url -->
			@if (array_key_exists($current_campaign_name,$urls))
				@if (array_key_exists($key,$urls[$current_campaign_name]))
					<div class="campaign-link">
						<a href="javascript: top.window.location.href='https://goo.gl/{{ $urls[$current_campaign_name][$key] }}';" onClick="_gaq.push(['_trackEvent', 'Order', 'Forward to Teespring', '{{ $current_campaign_name }}']);">Order here: https://goo.gl/{{ $urls[$current_campaign_name][$key] }}</a>
					</div>
				@endif
			@endif
		</div>
		<div class="clearfix margin-top-10"></div>
		<div><h4>{{ $type->description }}</h4></div>
		<div class="clearfix"></div>
		@if (array_key_exists($current_campaign_name,$urls))
			@if (array_key_exists($key,$urls[$current_campaign_name]))
				<div class="addthis_sharing_toolbox pull-right" data-url="https://goo.gl/{{ $urls[$current_campaign_name][$key] }}" data-title="Awesome tee with my name on it. Feel free to buy it for me at https://goo.gl/{{ $urls[$current_campaign_name][$key] }}"></div>
			@else
				<div class="addthis_sharing_toolbox pull-right" data-url="{{base_path()}}/share/?copy={{$current_campaign_name}}" data-title="Awesome tee with my name on it. Feel free to buy it for me at {{base_path()}}/share/?copy={{$current_campaign_name}}"></div>
			@endif
		@else
			<div class="addthis_sharing_toolbox pull-right" data-url="{{base_path()}}/share/?copy={{$current_campaign_name}}" data-title="Awesome tee with my name on it. Feel free to buy it for me at {{base_path()}}/share/?copy={{$current_campaign_name}}"></div>
		@endif
		<hr>
		<div class="pi-price">${{ $type->price }}</div>
		@if (array_key_exists($current_campaign_name,$urls))
			@if (array_key_exists($key,$urls[$current_campaign_name]))
				<a href="javascript: createorder('{{ $key }}', undefined, true)" onClick="_gaq.push(['_trackEvent', 'Order', 'Forward to Teespring', '{{ $current_campaign_name }}']);" class="btn btn-primary add2cart">Order</a>
			@else
				<a href="javascript: createorder('{{ $key }}')" onClick="_gaq.push(['_trackEvent', 'Order', 'Join Campaign', '{{ $current_campaign_name }}']);" class="btn btn-default add2cart">Order</a>
			@endif
		@else
			<a href="javascript: createorder('{{ $key }}')" onClick="_gaq.push(['_trackEvent', 'Order', 'Create Campaign', '{{ $current_campaign_name }}']);" class="btn btn-default add2cart">Order</a>
		@endif
	</div>
</div>