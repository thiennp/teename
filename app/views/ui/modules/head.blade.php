<!doctype html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<title>Your name's Tee</title>
		<meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<link rel="shortcut icon" href="favicon.ico">
		<!-- Fonts START -->
		{{ HTML::style('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all') }}
		{{ HTML::style('https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all') }}
		<!-- Fonts END -->
		<!-- Global styles START -->
		{{ HTML::style($base.'global/plugins/font-awesome/css/font-awesome.min.css') }}
		{{ HTML::style($base.'global/plugins/bootstrap/css/bootstrap.min.css') }}
		<!-- Global styles END -->
		
		<!-- Page level plugin styles START -->
		{{ HTML::style($base.'global/plugins/fancybox/source/jquery.fancybox.css') }}
		<!-- Page level plugin styles END -->
		<!-- Theme styles START -->
		{{ HTML::style($base.'global/css/components.css') }}
		{{ HTML::style($base.'frontend/layout/css/style.css') }}
		{{ HTML::style($base.'frontend/pages/css/style-shop.css') }}
		{{ HTML::style($base.'frontend/layout/css/style-responsive.css') }}
		{{ HTML::style($base.'frontend/layout/css/themes/red.css') }}
		{{ HTML::style($base.'frontend/layout/css/custom.css') }}
		<!-- {{ HTML::style('frontend/layout/css/custom.css') }} -->
		@foreach ($googlefonts as $key => $font)
			{{ HTML::style('https://fonts.googleapis.com/css?family='.$font) }}
		@endforeach
		<!-- Theme styles END -->
	</head>
	<body class="ecommerce">
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-53f0f3347ef255dc" async="async"></script>