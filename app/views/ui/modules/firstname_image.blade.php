{{array_key_exists($current_campaign_name,$urls)}}
<div class="tee-custom tee-custom-{{ $key }} copy-html" style="left: {{ $copy->custom->left+$copy->left }}px; -ms-transform: rotate({{ $copy->custom->rotate }}deg); -webkit-transform: rotate({{ $copy->custom->rotate }}deg); transform: rotate({{ $copy->custom->rotate }}deg); font-family: {{ $copy->custom->font }}; top: {{ $type->top+$copy->top+$copy->custom->top }}px; font-size: {{ $copy->custom->size }}px">
	{{ $current_name }}
</div>
@if ($first_name)
	<img src="{{ $base }}copy-{{ $key+1 }}.png" class="tee-copy" width="{{ $copy->width }}" style="left: {{ $copy->left }}px; top: {{ $type->top+$copy->top }}px" />
@else
	<div class="tee-loading">Loading...</div>
@endif
<img src="{{ $base }}type-{{ $i+1 }}.png" class="tee-shirt" width="196" />
<div style="background: #{{ $type->colors[$key] }}" class="tee-silk"></div>