<div class="col-xs-4 col-xs-12">
	<div class="product-item">
		<div class="pi-img-wrapper copy-1 tee-type-{{ $i }}">
			<div class="tee-custom tee-custom-{{ $key }} copy-html" style="left: {{ $copy->custom->left+$copy->left }}px; -ms-transform: rotate({{ $copy->custom->rotate }}deg); -webkit-transform: rotate({{ $copy->custom->rotate }}deg); transform: rotate({{ $copy->custom->rotate }}deg); font-family: {{ $copy->custom->font }}; top: {{ $type->top+$copy->top+$copy->custom->top }}px; font-size: {{ $copy->custom->size }}px">
				{{ $splited_name['name'] }}
			</div>
			<img src="{{ $base }}copy-{{ $key+1 }}.png" class="tee-copy tee-copy-{{ $key+1 }}" width="{{ $copy->width }}" style="left: {{ $copy->left }}px; top: {{ $type->top+$copy->top }}px" />
			<img src="{{ $base }}type-{{ $i+1 }}.png" class="tee-shirt" width="196" />
			<div style="background: #{{ $type->colors[$key] }}" class="tee-silk"></div>
			@if (array_key_exists($splited_name['campaign_name'],$urls))
				@if (array_key_exists($key,$urls[$splited_name['campaign_name']]))
					<div class="campaign-link">
						<a href="javascript: top.window.location.href='https://goo.gl/{{ $urls[$splited_name['campaign_name']][$key] }}';" onClick="_gaq.push(['_trackEvent', 'Order', 'Forward to Teespring', '{{ $splited_name['campaign_name'] }}']);">Order here: https://goo.gl/{{ $urls[$splited_name['campaign_name']][$key] }}</a>
					</div>
				@endif
			@endif
		</div>
		<div class="clearfix margin-top-10"></div>
		<div><h4>{{ $type->description }}</h4></div>
		<div class="clearfix"></div>
		@if (array_key_exists($splited_name['campaign_name'],$urls))
			@if (array_key_exists($key,$urls[$splited_name['campaign_name']]))
				<div class="addthis_sharing_toolbox pull-right" data-url="https://goo.gl/{{ $urls[$splited_name['campaign_name']][$key] }}" data-title="Awesome tee with my name on it. Feel free to buy it for me at https://goo.gl/{{ $urls[$splited_name['campaign_name']][$key] }}"></div>
			@else
				<div class="addthis_sharing_toolbox pull-right" data-url="{{base_path()}}/share/?copy={{$splited_name['name']}}" data-title="Awesome tee with my name on it. Feel free to buy it for me at {{base_path()}}/share/?copy={{$splited_name['name']}}"></div>
			@endif
		@else
			<div class="addthis_sharing_toolbox pull-right" data-url="{{base_path()}}/share/?copy={{$splited_name['name']}}" data-title="Awesome tee with my name on it. Feel free to buy it for me at {{base_path()}}/share/?copy={{$splited_name['name']}}"></div>
		@endif
		<hr>
		<div class="pi-price">${{ $type->price }}</div>
		@if (array_key_exists($splited_name['campaign_name'],$urls))
			@if (array_key_exists($key,$urls[$splited_name['campaign_name']]))
				<a href="javascript: createorder('{{ $key }}', '{{ $splited_name['campaign_name'] }}', true)" onClick="_gaq.push(['_trackEvent', 'Order', 'Forward to Teespring', '{{ $splited_name['campaign_name'] }}']);" class="btn btn-primary add2cart">Order</a>
			@else
				<a href="javascript: createorder('{{ $key }}', '{{ $splited_name['campaign_name'] }}')" onClick="_gaq.push(['_trackEvent', 'Order', 'Join Campaign', '{{ $splited_name['campaign_name'] }}']);" class="btn btn-default add2cart">Order</a>
			@endif
		@else
			<a href="javascript: createorder('{{ $key }}', '{{ $splited_name['campaign_name'] }}')" onClick="_gaq.push(['_trackEvent', 'Order', 'Create Campaign', '{{ $splited_name['campaign_name'] }}']);" class="btn btn-default add2cart">Order</a>
		@endif
	</div>
</div>