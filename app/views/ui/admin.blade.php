@include('modules/head')
@include('modules/header')
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h1>CAMPAIGNS</h1>
				<a href="{{ $base }}ktadmin/config">Config</a>
				|
				<a href="{{ $base }}ktadmin/copy">Add image to copy</a>
				|
				<a href="{{ $base }}ktadmin/user">User</a>
			</div>
			<div class="col-md-3">
				<a href="{{ $base }}logout"><button type="button" class="btn btn-danger pull-right">Logout</button></a>
			</div>
		</div>
		<hr>
		<div class="row margin-bottom-40">
			<div class="col-xs-12">
				<div class="row product-list">
					@foreach ($campaigns as $key => $campaign)
					<div class="col-xs-4 col-xs-12">
						<div class="product-item">
							<div style="text-align: center">
								<h2>{{ $campaign->id }}</h2>
							</div>
							<div class="pi-img-wrapper copy-1">
								<img src="{{ $base }}copy-{{ $campaign->style+1 }}.png" class="tee-copy" width="{{ $copies[$campaign->style]->width }}" style="left: {{ $copies[$campaign->style]->left }}px; top: {{ $types[0]->top+$copies[$campaign->style]->top }}px" >
								<img src="{{ $base }}type-1.png" class="tee-shirt" width="196" />
								<div style="background: #222" class="tee-silk"></div>
							</div>
							<div class="clearfix margin-top-10"></div>
							<h4><small>Custom copy:</small> <strong>{{ $campaign->copy }}</strong></h4>
							<form method="POST" action="{{ $base }}campaign/{{$campaign->id}}" role="form" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12">
										<div class="input-group">
											<span class="input-group-addon">
												https://goo.gl/
											</span>
											{{ Form::text('url', $campaign->url, ['class' => "form-control"]) }}
											<span class="input-group-btn">
												{{ Form::submit('Submit', ['class' => "btn btn-primary"]) }}
											</span>
										</div>
									</div>
									<div class="clearfix margin-bottom-10"></div>
									<div class="col-md-6">
										<h4>
											Custom image link
										</h4>
									</div>
									<div class="col-md-6 pull-right">
										<a href="{{ $base }}ktadmin/campaign/{{ $campaign->id }}">View <em>({{$campaign->count}} users)</em></a>
									</div>
									<div class="clearfix"></div>
									<div class="col-md-4">
										{{ Form::text('img_custom_1', $campaign['links']->link_1, ['class' => "form-control", 'placeholder' => "Link 1"]) }}
									</div>
									<div class="col-md-4">
										{{ Form::text('img_custom_2', $campaign['links']->link_2, ['class' => "form-control", 'placeholder' => "Link 2"]) }}
									</div>
									<div class="col-md-4">
										{{ Form::text('img_custom_3', $campaign['links']->link_3, ['class' => "form-control", 'placeholder' => "Link 3"]) }}
									</div>
								</div>
							</form>
							<div class="clearfix margin-top-10"></div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
