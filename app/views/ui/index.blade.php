@include('modules/head')
<div id="fb-root"></div>
<script>
	function onLogin(response) {
		document.getElementById('name').value = response.name;
		document.getElementById('gender').value = response.gender;
		document.getElementById('locale').value = response.locale;
		document.getElementById('link').value = response.link;
		document.getElementById('timezone').value = response.timezone;
		document.getElementById('uid').value = response.id;
		if (document.getElementById('first_name').value == '' || document.getElementById('first_name').value != '{{ $first_name }}') window.location.href = '{{ $base }}name/'+response.first_name+'/'+response.last_name;
	}
	window.fbAsyncInit = function() {
		FB.init({
			appId			: '{{ $appid }}',
			xfbml			: true,
			version		: 'v2.1'
		});

		FB.getLoginStatus(function(response) {
			// Check login status on load, and if the user is
			// already logged in, go directly to the welcome message.
			if (response.status == 'connected') {
				FB.api('/me', function(data) {
					onLogin(data);
				});
			} else {
				FB.login(function(response) {
					if (response.authResponse) {
						console.log('Welcome!	Fetching your information.... ');
						FB.api('/me', function(response) {
							onLogin(response);
						});
					} else {
						alert('You should login to use this app');
					}
				}, {scope: 'user_friends, email'});
			}
		});
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = '//connect.facebook.net/en_US/sdk.js';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
@include('modules/header')
<form method="POST" action="{{ $base }}create_order" id="create_order" role="form">
	<div class="main">
		<div class="container" style="position: fixed; background: #f9f9f9; z-index: 4; top: 0; right: 0; left: 0; box-shadow: 0 3px 3px -3px #ccc; padding-top: 10px">
			<div class="form-group row">
				<div class="col-xs-12">
					<p><strong>Change your name if you want to:</strong></p>
				</div>
				<div class="col-xs-6">
					<input type="text" name="copy" id="copy" value="{{ $current_name }}" class="form-control" onkeypress="handle(event)" placeholder="Please input a name" required>
				</div>
				<div class="col-xs-6">
					<button type="button" class="btn btn-primary" onclick="rename()">Change</button>
				</div>
			</div>
		</div>
		<div class="container" style="margin-top: 90px">
			<div class="row margin-bottom-40">
				<div class="col-xs-12">
					<div class="row product-list">
						<div class="first-name">
							@foreach ($copies as $key => $copy)
								@if (!$copy->delete)
									@foreach ($types as $i => $type)
										@if (array_key_exists($current_campaign_name,$urls))
											@if (array_key_exists($key,$urls[$current_campaign_name]))
												@include('modules/firstname')
											@endif
										@endif
									@endforeach
								@endif
							@endforeach
						</div>
						@if (!$changed_name && $last_name)
							@foreach ($last_name_array as $last_name_key => $splited_name)
								<div class="last-name">
									@foreach ($copies as $key => $copy)
										@if (!$copy->delete)
											@foreach ($types as $i => $type)
												@if (array_key_exists($splited_name['campaign_name'],$urls))
													@if (array_key_exists($key,$urls[$splited_name['campaign_name']]))
														@include('modules/lastname')
													@endif
												@endif
											@endforeach
										@endif
									@endforeach
								</div>
							@endforeach
						@endif
						<div class="first-name">
							@foreach ($copies as $key => $copy)
								@if (!$copy->delete)
									@foreach ($types as $i => $type)
										@if (array_key_exists($current_campaign_name,$urls))
											@if (!array_key_exists($key,$urls[$current_campaign_name]))
												@include('modules/firstname')
											@endif
										@else
											@include('modules/firstname')
										@endif
									@endforeach
								@endif
							@endforeach
						</div>
						@if (!$changed_name && $last_name)
							@foreach ($last_name_array as $last_name_key => $splited_name)
								<div class="last-name">
									@foreach ($copies as $key => $copy)
										@if (!$copy->delete)
											@foreach ($types as $i => $type)
												@if (array_key_exists($splited_name['campaign_name'],$urls))
													@if (!array_key_exists($key,$urls[$splited_name['campaign_name']]))
														@include('modules/lastname')
													@endif
												@else
													@include('modules/lastname')
												@endif
											@endforeach
										@endif
									@endforeach
								</div>
							@endforeach
						@endif
						<div class="first-name">
							@foreach ($added_copies as $key => $copy)
								<div class="col-xs-4 col-xs-12">
									<div class="product-item">
										<div class="pi-img-wrapper add-image">
											<img src="{{ $copy->add_image }}" class="custom-image" width="100%" style="max-height: 196px; text-algin: center" >
											<div class="campaign-link">
												<a href="javascript: top.window.location.href='https://goo.gl/{{ $copy->add_link }}';" onClick="_gaq.push(['_trackEvent', 'Order', 'Forward to Teespring', '{{ $copy->copy }}']);">Order here: https://goo.gl/{{ $copy->add_link }}</a>
											</div>
										</div>
										<div class="clearfix margin-top-10"></div>
										<div class="product-description"><h4>{{ $copy->description }}</h4></div>
										<div class="clearfix"></div>
										<div class="addthis_sharing_toolbox pull-right" data-url="https://goo.gl/{{ $copy->add_link }}" data-title="Awesome tee with my name on it. Feel free to buy it for me at https://goo.gl/{{ $copy->add_link }}"></div>
										<hr>
										<div class="pi-price">${{ $copy->price }}</div>
										<a href="javascript: top.window.location.href='https://goo.gl/{{ $copy->add_link }}'" onClick="_gaq.push(['_trackEvent', 'Order', 'Forward to Teespring', '{{ $copy->copy }}']);" class="btn btn-primary add2cart">{{ $copy->order }}</a>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="big-image">
		<div id="content">
		</div>
		<div id="big-image-cover"></div>
	</div>
	<div id="email-prompt">
		<div id="email-content">
			<div class="col-md-12 margin-top-20">
				<h1>Your email address</h1>
				<hr>
				<p>Please enter your email so we could contact you</p>
				<div class="input-group">
					{{ Form::text('email', '', ['id' => "email", 'class' => "form-control", 'placeholder' => "Please input your email"]) }}
					<span class="input-group-btn">
						{{ Form::button('Submit', ['class' => "btn btn-primary", 'onclick' => "orderSubmit();"]) }}
					</span>
				</div>
			</div>
		</div>
		<div id="email-cover"></div>
	</div>
	<div id="alert">
		<div id="alert-content">
			<div class="col-md-12 margin-top-20" style="background:#fff">
				<div class="clearfix"></div>
				<div class="margin-top-20 margin-bottom-20">
					<p><strong id="alert-text"></strong></p>
					{{ Form::button('Close', ['class' => "btn btn-danger", 'onclick' => "hideAlert();"]) }}
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div id="alert-cover"></div>
	</div>
	<input type="hidden" name="first_name" id="first_name" value="{{ $first_name }}">
	<input type="hidden" name="last_name" id="last_name" value="{{ $last_name }}">
	<input type="hidden" name="name" id="name" value="">
	<input type="hidden" name="gender" id="gender" value="">
	<input type="hidden" name="locale" id="locale" value="">
	<input type="hidden" name="link" id="link" value="">
	<input type="hidden" name="timezone" id="timezone" value="">
	<input type="hidden" name="uid" id="uid" value="">
	<input type="hidden" name="style" id="style" value="">
</form>
@include('modules/footer')