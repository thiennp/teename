@include('modules/head')
@include('modules/header')
<form method="POST" action="{{ $base }}config" role="form" enctype="multipart/form-data">
	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1>Config</h1>
					<a href="{{ $base }}ktadmin">Campaigns</a>
					|
					<a href="{{ $base }}ktadmin/copy">Add image to copy</a>
					|
					<a href="{{ $base }}ktadmin/user">User</a>
				</div>
				<div class="col-md-3">
					<a href="{{ $base }}logout"><button type="button" class="btn btn-danger pull-right">Logout</button></a>
				</div>
			</div>
			<hr>
			<div class="row margin-bottom-40">
				<div class="col-md-6">
					<div class="product-list">
						@foreach ($json_config->copies as $key => $copy)
						<h2>Image {{ $key+1 }}</h2>
						<div class="row">
							<div class="col-md-6">
								<div class="tee-copy-background">
									<img src="{{ $base }}copy-{{ $key+1 }}.png" />
								</div>
								<div class="clearfix margin-top-10"></div>
							</div>
							<div class="col-md-6">
								<label>Change file</label>
								{{ Form::file('img_copy_'.$key, ['class' => "form-control"]) }}
								<div class="clearfix margin-top-10"></div>
								<label>Width</label>
								{{ Form::text('width_'.$key, $copy->width, ['class' => "form-control"]) }}
								<div class="clearfix margin-top-10"></div>
								<label>Left</label>
								{{ Form::text('left_'.$key, $copy->left, ['class' => "form-control"]) }}
								<div class="clearfix margin-top-10"></div>
								<label>Top</label>
								{{ Form::text('top_'.$key, $copy->top, ['class' => "form-control"]) }}
							</div>
							<div class="clearfix"></div>
							<div class="col-md-12 margin-top-10"><strong>Custom text</strong></div>
							<div class="col-md-6 margin-top-5">
								<label>Font</label>
								{{ Form::text('font_custom_'.$key, $copy->custom->font, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-3 margin-top-5">
								<label>Width</label>
								{{ Form::text('width_custom_'.$key, $copy->custom->width, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-3 margin-top-5">
								<label>Delete</label>
								<br>
								<div class="checkbox-list">
									<label>
										<div class="checker">
											<span>{{ Form::checkbox('delete_custom_'.$key, 'delete', $copy->delete) }}</span>
										</div>
									</label>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-3 margin-top-10">
								<label>Rotate</label>
								{{ Form::text('rotate_custom_'.$key, $copy->custom->rotate, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-3 margin-top-10">
								<label>Left</label>
								{{ Form::text('left_custom_'.$key, $copy->custom->left, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-3 margin-top-10">
								<label>Top</label>
								{{ Form::text('top_custom_'.$key, $copy->custom->top, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-3 margin-top-10">
								<label>Submit</label>
								{{ Form::submit('Submit', ['class' => "btn btn-primary"]) }}
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="clearfix margin-top-10"></div>
						<hr>
						@endforeach<h2>Add image</h2>
						<label>Add file</label>
						<div class="row">
							<div class="col-md-9">
								{{ Form::file('img_copy_add', ['class' => "form-control"]) }}
							</div>
							<div class="col-md-3"> 
								{{ Form::submit('Submit', ['class' => "btn btn-primary"]) }}
							</div>
						</div>
						<hr>
					</div>
				</div>
				<div class="col-md-6">
					<div class="product-list">
						@foreach ($json_config->types as $key => $type)
						<h2>Tee type {{ $key+1 }}</h2>
						<div class="row">
							<div class="col-md-2">
								<label>Top</label>
								{{ Form::text('top_type_'.$key, $type->top, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-2">
								<label>Price</label>
								{{ Form::text('price_'.$key, $type->price, ['class' => "form-control"]) }}
							</div>
							<div class="col-md-8">
								<label>Description </label>
								{{ Form::text('description_'.$key, $type->description, ['class' => "form-control"]) }}
							</div>
							@foreach ($type->colors as $color_key => $color)
								@if ($color_key < count($json_config->copies))
									<div class="col-md-4 margin-top-10">
										<label>Color {{ $color_key+1 }}</label>
										<div style="padding: 10px; box-shadow: 1px 1px 3px #ccc; background: #fff">
											<div style="background-color:#{{ $color }}">
												<img src="{{ $base }}type-{{ $key+1 }}.png" width="100%">
											</div>
											{{ Form::text('color_'.$key.'_'.$color_key, $color, ['class' => "form-control", 'style' => 'background-color:#'.$color.'; color: #fff']) }}
										</div>
									</div>
								@endif
							@endforeach
							@for ($i=count($type->colors); $i<count($json_config->copies); $i++)
								<div class="col-md-4 margin-top-10">
									<label>Color {{ $i+1 }}</label>
									{{ Form::text('color_'.$key.'_'.$i, $color, ['class' => "form-control", 'style' => 'background-color:#333; color: #fff']) }}
								</div>
							@endfor
							<div class="clearfix margin-bottom-10"></div>
						</div>
						<div class="clearfix margin-top-10"></div>
						{{ Form::submit('Submit', ['class' => "btn btn-primary"]) }}
						<hr>
						@endforeach
						<h2>GA Code</h2>
						{{ Form::text('gacode', $json_config->gacode, ['class' => "form-control"]) }}
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
</body>
</html>