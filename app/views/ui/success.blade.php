@include('modules/head')
@include('modules/header')
<div class="main">
	<div class="container">
		<div class="row margin-bottom-40">
			<div class="col-xs-12">
				<div class="form-group row">
					<div class="col-xs-12">
						<h1>Thank you</h1>
						<p><strong>
							Your request has been processed, your campaign will be running soon. We'll let you know whenever it's ready. Keep in touch!</strong></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>