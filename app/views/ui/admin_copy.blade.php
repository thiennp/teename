@include('modules/head')
@include('modules/header')
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h1>CAMPAIGNS</h1>
				<a href="{{ $base }}ktadmin">Campaigns</a>
				|
				<a href="{{ $base }}ktadmin/config">Config</a>
				|
				<a href="{{ $base }}ktadmin/user">User</a>
			</div>
			<div class="col-md-3">
				<a href="{{ $base }}logout"><button type="button" class="btn btn-danger pull-right">Logout</button></a>
			</div>
		</div>
		<hr>
		<div class="row margin-bottom-40">
			<div class="col-xs-12">
				<div class="row product-list">
					<div class="col-xs-4 col-xs-12">
						<div class="product-item">
							<div style="text-align: center">
								<h2>Add copy</h2>
							</div>
							<div class="clearfix margin-top-10"></div>
							<form method="POST" action="{{ $base }}copy/0" role="form" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-6">
										{{ Form::text('copy', '', ['class' => "form-control", 'placeholder' => "Text"]) }}
									</div>
									<div class="col-md-6">
										{{ Form::text('add_image', '', ['class' => "form-control", 'placeholder' => "Image URL"]) }}
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-12">
										<div class="input-group">
											<span class="input-group-addon">
												https://goo.gl/
											</span>
											{{ Form::text('add_link', '', ['class' => "form-control", 'placeholder' => "custom URL"]) }}
										</div>
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-6">
										{{ Form::text('price', '', ['class' => "form-control", 'placeholder' => "price ($)"]) }}
									</div>
									<div class="col-md-6">
										{{ Form::text('order', 'Order', ['class' => "form-control", 'placeholder' => "Order"]) }}
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-12">
										{{ Form::textarea('description', '', ['class' => "form-control", 'placeholder' => "description", 'rows' => 6]) }}
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-12">
										{{ Form::submit('Submit', ['class' => "btn btn-primary pull-right"]) }}
									</div>
								</div>
							</form>
							<div class="clearfix margin-top-10"></div>
						</div>
					</div>
					@foreach ($copies as $key => $copy)
					<div class="col-xs-4 col-xs-12">
						<div class="product-item">
							<div style="text-align: center">
								<h2>{{ $copy->copy }}</h2>
							</div>
							<div class="clearfix margin-top-10"></div>
							<form method="POST" action="{{ $base }}copy/{{$copy->id}}" role="form" enctype="multipart/form-data">
								<div class="row">
									<div class="col-md-12">
										<div class="input-group">
											<span class="input-group-addon">
												https://goo.gl/
											</span>
											{{ Form::text('add_link', $copy->add_link, ['class' => "form-control", 'placeholder' => "Add link"]) }}
										</div>
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-12">
										{{ Form::text('add_image', $copy->add_image, ['class' => "form-control", 'placeholder' => "Add image link"]) }}
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-6">
										{{ Form::text('price', $copy->price, ['class' => "form-control", 'placeholder' => "price ($)"]) }}
									</div>
									<div class="col-md-6">
										{{ Form::text('order', $copy->order, ['class' => "form-control", 'placeholder' => "Order"]) }}
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-12">
										{{ Form::textarea('description', $copy->description, ['class' => "form-control", 'placeholder' => "description", 'rows' => 6]) }}
									</div>
								</div>
								<div class="row margin-top-10">
									<div class="col-md-6">
										<span>{{ Form::checkbox('delete', 'delete', 0) }} Delete</span>
									</div>
									<div class="col-md-6">
										{{ Form::submit('Submit', ['class' => "btn btn-primary pull-right"]) }}
									</div>
								</div>
							</form>
							<div class="clearfix margin-top-10"></div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
