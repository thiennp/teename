@include('modules/head')
@include('modules/header')
	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<h1>CAMPAIGN USERS</h1>
					<a href="{{ $base }}ktadmin">Campaign list</a>
					|
					<a href="{{ $base }}ktadmin/copy">Add image to copy</a>
					|
					<a href="{{ $base }}ktadmin/user">User</a>
				</div>
				<div class="col-md-3">
					<a href="{{ $base }}logout"><button type="button" class="btn btn-danger pull-right">Logout</button></a>
				</div>
			</div>
			<hr>
			<div class="row margin-bottom-40">
				<div class="col-md-1"><strong>Id</strong></div>
				<div class="col-md-3"><strong>Name</strong></div>
				<div class="col-md-3"><strong>Email</strong></div>
				<div class="col-md-2"><strong>UID</strong></div>
				<div class="clearfix"></div>
				<hr>
				@foreach ($campaign_users as $key => $campaign_user)
					<div class="col-md-1">{{ $campaign_user->id }}</div>
					<div class="col-md-3">{{ $campaign_user->name }}</div>
					<div class="col-md-3">{{ $campaign_user->email }}</div>
					<div class="col-md-3">{{ $campaign_user->uid }}</div>
					<div class="clearfix"></div>
					<hr>
				@endforeach
			</div>
		</div>
	</div>
</body>
</html>