@include('modules/head')
@include('modules/header')
	<form method="POST" action="{{ $base }}login" id="login" role="form">
		<div class="main">
			<div class="container">
				<h1>LOGIN</h1>
				<hr>
				@if ($error)
				<div class="alert alert-danger">Wrong username or password</div>
				@endif
				<div class="row margin-bottom-40">
					<div class="col-xs-12">
						<div class="form-group row">
							<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 margin-top-10">
								<input type="text" name="username" id="username" placeholder="username" class="form-control">
							</div>
							<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 margin-top-10">
								<input type="password" name="password" id="password" placeholder="password" class="form-control">
							</div>
							<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2 margin-top-10">
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</body>
</html>