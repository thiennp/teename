@include('modules/head')
@include('modules/header')
<div class="main">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<h1>CAMPAIGNS</h1>
				<a href="{{ $base }}ktadmin">Campaigns</a>
				|
				<a href="{{ $base }}ktadmin/config">Config</a>
				|
				<a href="{{ $base }}ktadmin/copy">Add image to copy</a>
			</div>
			<div class="col-md-3">
				<a href="{{ $base }}logout"><button type="button" class="btn btn-danger pull-right">Logout</button></a>
			</div>
		</div>
		<hr>
		<div class="row margin-bottom-40">
			<div class="col-xs-12">
				<div class="product-list">
					<table class="table table-hover">
						<tr>
							<th>Id</th>
							<th>uid</th>
							<th>Email</th>
							<th>First name</th>
							<th>Last name</th>
							<th>Gender</th>
							<th>Locale</th>
							<th>Facebook</th>
						</tr>
						@foreach ($users as $key => $user)
						<tr>
							<td>{{ $user->id }}</td>
							<td>{{ $user->uid }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->first_name }}</td>
							<td>{{ $user->last_name }}</td>
							<td>{{ $user->gender }}</td>
							<td>{{ $user->locale }}</td>
							<td><a href="{{ $user->link }}">Facebook</a></td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
